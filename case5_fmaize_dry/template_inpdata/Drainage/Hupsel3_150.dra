**********************************************************************************
* Filename: Hupsel.DRA                       
* Contents: SWAP 3.0 - Data for basic and extended drainage
**********************************************************************************
* Comment area:                                                       
*
**********************************************************************************


*** BASIC DRAINAGE SECTION ***

**********************************************************************************
* Part 0: General
*
  DRAMET = 3 ! Switch, method of lateral drainage calculation: 
*              METHOD 1 = Use table of drainage flux - groundwater level relation      
*              METHOD 2 = Use drainage formula of Hooghoudt or Ernst               
*              METHOD 3 = Use drainage/infiltration resistance, multi-level if needed

  SWDIVD = 1 ! Calculate vertical distribution of drainage flux in groundwater [Y=1, N=0]

* If SWDIVD = 1, specify anisotropy factor COFANI (horizontal/vertical saturated hydraulic 
* conductivity) for each soil layer (maximum MAHO), [0..1000 -, R] :
  COFANI =    1000.0  1000.0  1000.0 
**********************************************************************************


**********************************************************************************
*
* METHOD 1 - Part 1: Table of drainage flux - groundwater level relation (DRAMET = 1)
*
* If SWDIVD = 1, specify the drain spacing:
  LM1 = 30.  ! Drain spacing, [1..1000 m, R]

* Specify drainage flux Qdrain [-100..1000 cm/d, R] as function of groundwater level 
* GWL [-1000.0..10.0 cm, R, negative below soil surface]; maximum of 25 records
* start with highest groundwater level: 

     GWL     Qdrain
   -20.0        0.5
   -100.        0.1
* End of table                                             
**********************************************************************************


**********************************************************************************
*
* METHOD 2 - Part 2: Drainage formula of Hooghoudt or Ernst (DRAMET = 2)
*
* Drain characteristics:
  LM2    =  1.      ! Drain spacing, [1..1000 m, R]
  WETPER =  30.0    ! Wet perimeter of the drain,  [0..1000 cm, R]
  ZBOTDR = -80.0    ! Level of drain bottom, [-1000..0 cm, R, neg. below soil surface]
  ENTRES =  20.0    ! Drain entry resistance, [0..1000 d, R]

* Soil profile characteristics:
*
  IPOS = 2   ! Position of drain:
*              1 = On top of an impervious layer in a homogeneous profile          
*              2 = Above an impervious layer in a homogeneous profile              
*              3 = At the interface of a fine upper and a coarse lower soil layer
*              4 = In the lower, more coarse soil layer
*              5 = In the upper, more fine soil layer                                             

* For all positions specify:
  BASEGW = -200.    ! Level of impervious layer, [-1d4..0 cm, R]
  KHTOP  =   25.    ! Horizontal hydraulic conductivity top layer, [0..1000 cm/d, R]

* In addition, in case IPOS = 3,4,5
  KHBOT  =  10.0    ! horizontal hydraulic conductivity bottom layer, [0..1000 cm/d, R]
  ZINTF  = -150.    ! Level of interface of fine and coarse soil layer, [-1d4..0 cm, R]

* In addition, in case IPOS = 4,5
  KVTOP  =   5.0    ! Vertical hydraulic conductivity top layer, [0..1000 cm/d, R]
  KVBOT  =  10.0    ! Vertical hydraulic conductivity bottom layer, [0..1000 cm/d, R]

* In addition, in case IPOS = 5
  GEOFAC =  4.8     ! Geometry factor of Ernst,  [0..100 -, R]
**********************************************************************************


**********************************************************************************
*
* METHOD 3 - Part 3: Drainage and infiltration resistance (DRAMET = 3)
*
*
  NRLEVS = 1        ! Number of drainage levels, [1..5, I]
*
* Option for interflow in highest drainage level (shallow system with short residence time)
  SWINTFL = 0       ! Switch for interflow [0,1, I]
* If SWINTFL = 1, then specify COFINTFLB and EXPINTFLB
  COFINTFLB = 0.5   ! Coefficient for interflow relation  [0.01..10.0 d, R]
  EXPINTFLB = 1.0   ! Exponent for interflow relation  [0.1..1.0 -, R]
**********************************************************************************


**********************************************************************************
* Part 3a: Drainage to level 1
*
  DRARES1 = 100.0 ! Drainage resistance, [10..1d5 d, R]
  INFRES1 = 100.0 ! Infiltration resistance, [0..1d5 d, R]
  SWALLO1 =   3   ! Switch, for allowance drainage/infiltration:
                  ! 1 = Drainage and infiltration are both allowed
                  ! 2 = Drainage is not allowed
                  ! 3 = Infiltration is not allowed                          
  SWDISLAY = 0

* If SWDIVD = 1 (drainage flux vertically distributed), specify the drain spacing:
  L1   = 20.      ! Drain spacing, [1..1000 m, R]

  ZBOTDR1 = -100.0 ! Level of drainage medium bottom, [-1000..0 cm, R]
  SWDTYP1 = 1     ! Type of drainage medium: 1 = drain tube, 2 = open channel

* In case of open channel (SWDTYP1 = 2), specify date DATOWL1 [dd-mmm-yyyy] and channel 
* water level LEVEL1 [cm, negative if below soil surface], maximum MAOWL records:

       DATOWL1   LEVEL1
   01-jan-1971    -50.0
   31-dec-2015    -50.0
* End of table
**********************************************************************************


**********************************************************************************
* Part 3b: Drainage to level 2
*
  DRARES2 = 100   ! Drainage resistance, [10..1E5 d, R]
  INFRES2 = 100   ! Infiltration resistance, [0..1E5 d, R]
  SWALLO2 =   1   ! Switch, for allowance drainage/infiltration:
                  ! 1 = Drainage and infiltration are both allowed
                  ! 2 = Drainage is not allowed
                  ! 3 = Infiltration is not allowed                          

* If SWDIVD = 1 (drainage flux vertically distributed), specify the drain spacing:
  L2 = 20.        ! Drain spacing, [1..1000 m, R]

  ZBOTDR2 = -90.0 ! Level of drainage medium bottom, [-1000..0 cm, R]
  SWDTYP2 = 2     ! Type of drainage medium: 1 = drain tube, 2 = open channel

* In case of open channel (SWDTYP2 = 2), specify date DATOWL2 [dd-mmm-yyyy] and channel 
* water level LEVEL2 [cm, negative if below soil surface], maximum MAOWL records:

       DATOWL2   LEVEL2
   12-jan-1981    -90.0
   14-dec-1981    -90.0
* End of table
**********************************************************************************


**********************************************************************************
* Part 3c: Drainage to level 3
*
  DRARES3 = 100   ! Drainage resistance, [10..1E5 d, R]
  INFRES3 = 100   ! Infiltration resistance, [0..1E5 d, R]
  SWALLO3 =   1   ! Switch, for allowance drainage/infiltration:
                  ! 1 = Drainage and infiltration are both allowed
                  ! 2 = Drainage is not allowed
                  ! 3 = Infiltration is not allowed                          

* If SWDIVD = 1 (drainage flux vertically distributed), specify the drain spacing:
  L3 = 20.        ! Drain spacing, [1..1000 m, R]

  ZBOTDR3 = -90.0 ! Level of drainage medium bottom, [-1000..0 cm, R]
  SWDTYP3 = 2     ! Type of drainage medium: 1 = drain tube, 2 = open channel

* In case of open channel (SWDTYP3 = 2), specify date DATOWL3 [dd-mmm-yyyy] and channel 
* water level LEVEL3 [cm, negative if below soil surface], maximum MAOWL records:

       DATOWL3   LEVEL3
   12-jan-1981    -90.0
   14-dec-1981    -90.0
* End of table
**********************************************************************************


**********************************************************************************
* Part 3d: Drainage to level 4
*
  DRARES4 = 100   ! Drainage resistance, [10..1E5 d, R]
  INFRES4 = 100   ! Infiltration resistance, [0..1E5 d, R]
  SWALLO4 =   1   ! Switch, for allowance drainage/infiltration:
                  ! 1 = Drainage and infiltration are both allowed
                  ! 2 = Drainage is not allowed
                  ! 3 = Infiltration is not allowed                          

* If SWDIVD = 1 (drainage flux vertically distributed), specify the drain spacing:
  L4 = 20.        ! Drain spacing, [1..1000 m, R]

  ZBOTDR4 = -90.0 ! Level of drainage medium bottom, [-1000..0 cm, R]
  SWDTYP4 = 2     ! Type of drainage medium: 1 = drain tube, 2 = open channel

* In case of open channel (SWDTYP4 = 2), specify date DATOWL4 [dd-mmm-yyyy] and channel 
* water level LEVEL4 [cm, negative if below soil surface], maximum MAOWL records:

       DATOWL4   LEVEL4
   12-jan-1981    -90.0
   14-dec-1981    -90.0
* End of table
**********************************************************************************


**********************************************************************************
* Part 3e: Drainage to level 5
*
  DRARES5 = 100   ! Drainage resistance, [10..1E5 d, R]
  INFRES5 = 100   ! Infiltration resistance, [0..1E5 d, R]
  SWALLO5 =   1   ! Switch, for allowance drainage/infiltration:
                  ! 1 = Drainage and infiltration are both allowed
                  ! 2 = Drainage is not allowed
                  ! 3 = Infiltration is not allowed                          

* If SWDIVD = 1 (drainage flux vertically distributed), specify the drain spacing:
  L5 = 20.        ! Drain spacing, [1..1000 m, R]

  ZBOTDR5 = -90.0 ! Level of drainage medium bottom, [-1000..0 cm, R]
  SWDTYP5 = 2     ! Type of drainage medium: 1 = drain tube, 2 = open channel

* In case of open channel (SWDTYP5 = 2), specify date DATOWL5 [dd-mmm-yyyy] and channel 
* water level LEVEL5 [cm, negative if below soil surface], maximum MAOWL records:

       DATOWL5   LEVEL5
   12-jan-1981    -90.0
   14-dec-1981    -90.0
* End of table
***********************************************************************************


*** EXTENDED DRAINAGE SECTION ***


********************************************************************************
* Part 0: Reference level

 ALTCU = 0.0 ! ALTitude of the Control Unit relative to reference level
*                 AltCu = 0.0 means reference level coincides with
*                 surface level [-300000..300000 cm, R] 

********************************************************************************
* Part 1a: drainage characteristics 
*
 NRSRF  = 2    ! number of subsurface drainage levels [1..5, I]
*
*** Table with physical characteristics of each subsurface drainage level:
*
* LEVEL   ! drainage level number [1..NRSRF, I]
* SWDTYP  ! type of drainage medium [open=0, closed=1] 
* L       ! spacing between channels/drains [1..1000 m, R]
* ZBOTDRE ! altitude of bottom of channel or drain [ALTCU-1000..ALTCU-0.01 cm,R]
* GWLINF  ! groundw. level for max. infiltr. [-1000..0 cm rel. to soil surf., R]
* RDRAIN  ! drainage resistance [1..100000 d, R]
* RINFI   ! infiltration resistance  [1..100000 d, R]
* Variables RENTRY, REXIT, WIDTHR and TALUDR must have realistic values when the
*          type of drainage medium is open (second column of this table:SWDTYP=0)
*          For closed pipe drains (SWDTYP=1) dummy values may be entered
* RENTRY  ! entry resistance  [1..100 d, R]
* REXIT   ! exit resistance   [1..100 d, R]
* WIDTHR  ! bottom width of channel [0..100 cm, R]
* TALUDR  ! side-slope (dh/dw) of channel [0.01..5, R]
*                                                                     
 LEV SWDTYP    L   ZBOTDRE GWLINF RDRAIN RINFI RENTRY REXIT  WIDTHR TALUDR
  1   0      250.0  1093.0 -350.0 150.0  4000.0  0.8    0.8  100.0  0.66
  2   0      200.0  1150.0 -300.0 150.0  1500.0  0.8    0.8  100.0  0.66 
* End_of_table
*
*
* Part 1b: Separate criteria for highest (shallow) drainage system
*
 SWNRSRF = 0     ! Switch to introduce rapid subsurface drainage [0..2, I]
*            0 = no rapid drainage
*            1 = rapid drainage in the highest drainage system (=NRSRF)
*                (implies adjustment of RDRAIN of highest drainage system)
*            2 = rapid drainage as interflow according to a power relation
*                (implies adjustment of RDRAIN of highest drainage system)
* When SWRNSRF = 1, then enter realistic values for rapid drainage
 RSURFDEEP    = 30.0   ! maximum resistance of rapid subsurface Drainage [0.001..1000.0 d, R]
 RSURFSHALLOW = 10.0   ! minimum resistance of Rapid subsurface Drainage [0.001..1000.0 d, R]
*
* When SWRNSRF = 2, then enter coefficients of power function
 COFINTFL = 0.1        ! coefficient of interflow relation [0.01..10.0 d-1, R]
 EXPINTFL = 0.5        ! exponent of interflow relation [0.1...1.0 -, R]
*
*
********************************************************************************
* Part 2a: Specification and control of surface water system
*
 SWSRF = 2 ! option for interaction with surface water system [1..3, I]
*            1 = no interaction with surface water system
*            2 = surf. water system is simulated with no separate primary system 
*            3 = surf. water system is simulated with separate primary system
********************************************************************************
*
********************************************************************************
* Part 2b: Surface water level of primary system 
*
* Only if SWSRF = 3 then the following table must be entered
* Table with Water Levels in the Primary system [max. = 52]:
* no levels above soil surface for primary system      
* 
* Water level in primary water course WLP [ALTCU-1000..ALTCU-0.01 cm, R] as function of
* DATE1 [dd-mmm-yyyy] 

       DATE1      WLP
 02-jan-1980    -100.
 14-jun-1980     -80.
 24-oct-1980    -120.
*End_of_table
********************************************************************************
*
********************************************************************************
* Part 2c: Surface water level of secondary system

* If SWSRF =  2 or 3  then the variable SWSEC must be entered

 SWSEC = 2 ! option for surface water level of secondary system [1..2, I]
*            1 = surface water level is input
*            2 = surface water level is simulated
********************************************************************************

********************************************************************************
* Part 3: surface water level in secondary water course is input
*
* Table with Water Levels in the Secondary system [max. = 52]:
* 
* Water level in secondary water course WLS [ALTCU-1000..ALTCU-0.01 cm, R] as function of
* DATE2 [dd-mmm-yyyy] 

       DATE2      WLS
 02-jan-1980    -100.
 14-jun-1980     -80.
 24-oct-1980    -120.
*End_of_table
********************************************************************************

********************************************************************************
* Part 4: surface water level is simulated
*
********************************************************************************
* Part 4a: Miscellaneous parameters
*    
 WLACT  = 1123.0 ! initial surface water level [ALTCU-1000..ALTCU cm,R]
 OSSWLM =   2.5  ! criterium for warning about oscillation [0..10 cm, R]
********************************************************************************
*
********************************************************************************
* Part 4b: management of surface water levels
*
 NMPER  =  4    ! number of management periods [1..10, I]
*
* For each management period specify:
* IMPER  index of management period [1..NMPER, I]
* IMPEND date that period ends [dd-mm-yyyy]
* SWMAN  type of water management [1..2, I]
*        1 = fixed weir crest
*        2 = automatic weir
* WSCAP  surface water supply capacity [0..100 cm/d, R]
* WLDIP  allowed dip of surf. water level, before starting supply [0..100 cm, R]
* INTWL  length of water-level adjustment period (SWMAN=2 only) [1..31 d, R]

 IMPER_4b          IMPEND    SWMAN   WSCAP   WLDIP   INTWL
        1     31-jan-1980        1    0.00     0.0       1
        2     01-apr-1980        2    0.00     5.0       1
        3     01-nov-1980        2    0.00     5.0       1
        4     31-dec-1980        1    0.00     0.0       1
*End_of_table
*
 SWQHR  = 1 ! option for type of discharge relationship [1..2, I]
*             1 = exponential relationship
*             2 = table
********************************************************************************
*
********************************************************************************
* Part 4c: exponential discharge relation (weir characteristics)
*             
* If SWQHR=1 and for ALL periods specify:
*
 SOFCU = 100.0  ! Size of the control unit [0.1..100000.0 ha, R]
*
* IMPER  index of management period [1..NMPER, I]
* HBWEIR weir crest; levels above soil surface are allowed, but simulated
*        surface water levels should remain below 100 cm above soil surface; 
*        the crest must be higher than the deepest channel bottom of the 
*        secondary system (ZBOTDR(1 or 2),  [ALTCU-ZBOTDR..ALTCU+100 cm,R].
*        If SWMAN = 2: HBWEIR represents the lowest possible weir position.
* ALPHAW alpha-coefficient of discharge formula [0.1..50.0, R]
* BETAW  beta-coefficient of discharge formula [0.5..3.0, R]

 IMPER_4c  HBWEIR  ALPHAW   BETAW
      1    1114.0    3.0    1.4765
      2    1110.0    3.0    1.4765
      3    1110.0    3.0    1.4765
      4    1114.0    3.0    1.4765
*End_of_table
********************************************************************************
*
********************************************************************************
* Part 4d: table discharge relation
*
 LABEL4d = 1 ! Do not modify
*
* If SWQHR=2 and for ALL periods specify:
*
* IMPER  index of management period [1..NMPER, I]
* ITAB   index per management period [1..10, I]
* HTAB   surface water level [ALTCU-1000..ALTCU+100 cm, R]
*        (first value for each period = ALTCU + 100 cm)
* QTAB   discharge [0..500 cm/d, R]
*        (should go down to a value of zero at a level that is higher than
*        the deepest channel bottom of secondary surface water system)
*
 IMPER_4d IMPTAB  HTAB    QTAB
    1       1      -75.0   2.0
*End_of_table
********************************************************************************
*
********************************************************************************
* Part 4e: automatic weir control
*
 LABEL4e = 1 ! Do not modify
*
* For the periods when SWMAN=2 specify next two tables:
*
*** Table #1
*
*
* IMPER  index of management period [1..NMPER, I]
* DROPR  maximum drop rate of surface water level [0..100 cm/d, positive, R]
*        if the value is set to zero, the parameter does not play
*        any role at all
* HDEPTH depth in soil profile for comparing with HCRIT 
*        [-100..0 cm below soil surface, R]
*
 IMPER_4E1 DROPR   HDEPTH
       2    0.0     -15.0
       3    0.0     -15.0
*End_of_table
*
*** Table #2
*
* IMPER   index of management period [1..NMPER, I]
* IPHASE  index per management period [1..10, I]
* WLSMAN  surface water level of phase IPHASE [ALTCU-500.0..ALTCU cm,R]
* GWLCRIT groundwater level of phase IPHASE,  max. value 
*         [-500..0 cm  below soil surface, R]
* HCRIT   critical pressure head, max. value, (at HDEPTH, see above)
*         for allowing surface water level [-1000..0 cm, neg., R]   
* VCRIT   critical unsaturated volume (min. value) for all
*         surface water level [0..20 cm, R]
*
*   Notes: 1) The zero's for the criteria on the first record are in fact    
*             dummy's, because under all circumstances the scheme will set  
*             the surface water level at least to wlsman(imper,1)
*          2) The lowest level of the scheme must still be above the
*             deepest channel bottom of the secondary surface water system
*
 IMPER_4E2 IMPPHASE WLSMAN GWLCRIT    HCRIT   VCRIT
         2     1    1114.0     0.0      0.0     0.0
         2     2    1124.0   -80.0      0.0     0.0
         2     3    1124.0   -90.0      0.0     0.0
         2     4    1154.0  -100.0      0.0     0.0
         3     1    1114.0     0.0      0.0     0.0
         3     2    1124.0   -80.0      0.0     0.0
         3     3    1124.0   -90.0      0.0     0.0
         3     4    1154.0  -100.0      0.0     0.0
*End_of_table
********************************************************************************

* End of .dra file!