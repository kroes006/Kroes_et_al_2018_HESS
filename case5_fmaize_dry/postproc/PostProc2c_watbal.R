# setwd("d:\\Kroes\\Prive\\Promotie\\artikel_VertDiscr_Kroes_Supit_etal\\test\\cases_sophy_star_maize_Level1_50cm\\postproc")

# file.remove(list.files(path="./res",full.names = TRUE))
dir.create("./res")

# waterbalans period 1971-2000
filnam <- list.files("./watbal",pattern=".watbal.csv",full.names = TRUE)
wba    <- read.csv(file = filnam[1], skip = 0)
plotnr <- as.integer(unlist(strsplit(filnam[1],"\\."))[4])
nyr    <- nrow(wba)
wba[1:(nyr),1] <- plotnr
watbal <- wba
for (i in (2:length(filnam)))  {
    wba    <- read.csv(file = filnam[i], skip = 0)
    plotnr <- as.integer(unlist(strsplit(filnam[i],"\\."))[4])
    nyr    <- nrow(wba)
    wba[1:(nyr),1] <- plotnr
    watbal <- rbind(watbal,wba)
}
wb <- watbal

# initialize
#watbal1<-as.data.frame(expand.grid(ipl=unique(wb1$ipl),yr=1971:2000))

# merge 
#wb <- merge(x=watbal1,y=wb1, all.x=TRUE)

# waterbalans per plot en per jaar
wbal<-data.frame(
                   plot = wb$ipl,
                   Jaar = wb$yr,
                   Neerslag = wb$Igrai+wb$Igsnow,
                   Irrigatie = wb$Igirr,
                   Infiltratie = wb$fldrin1+wb$fldrin2+wb$fldrin3+wb$flindr4+wb$fldrin5,
                   Kwel = wb$flbtin,
                   VerdampingGewas = wb$flev,
                   VerdampingInterc = wb$evicpr+wb$evicir,
                   VerdampingBodem = wb$evso+wb$evsubl,
                   Runoff = wb$runoff,
                   Drainage = wb$fldrou1+wb$fldrou2+wb$fldrou3+wb$fldrou4+wb$fldrou5,
                   Wegzijging = wb$flbtou,
                   BergingDelta = wb$deltast+wb$deltapn,
                   BalDev = wb$badev)
filnam = "./res/Waterbalans_plot_jaar.csv"
write.table(x = wbal, file = filnam , sep = ",", quote = FALSE,
              row.names = FALSE, col.names = TRUE)

#  waterbalans per plot (arithmetic average in mm water)
MeanA_Plotnr <- tapply(X = wbal$plot, INDEX = wbal$plot, FUN = mean)
MeanA_Neerslag <- tapply(X = wbal$Neerslag, INDEX = wbal$plot, FUN = mean)
MeanA_Irrigatie <- tapply(X = wbal$Irrigatie, INDEX = wbal$plot, FUN = mean)
MeanA_Infiltratie <- tapply(X = wbal$Infiltratie, INDEX = wbal$plot, FUN = mean)
MeanA_Kwel <- tapply(X = wbal$Kwel, INDEX = wbal$plot, FUN = mean)
MeanA_VerdampingGewas <- tapply(X = wbal$VerdampingGewas, INDEX = wbal$plot, FUN = mean)
MeanA_VerdampingInterc <- tapply(X = wbal$VerdampingInterc, INDEX = wbal$plot, FUN = mean)
MeanA_VerdampingBodem <- tapply(X = wbal$VerdampingBodem, INDEX = wbal$plot, FUN = mean)
MeanA_Runoff <- tapply(X = wbal$Runoff, INDEX = wbal$plot, FUN = mean)
MeanA_Drainage <- tapply(X = wbal$Drainage, INDEX = wbal$plot, FUN = mean)
MeanA_Wegzijging <- tapply(X = wbal$Wegzijging, INDEX = wbal$plot, FUN = mean)
MeanA_BergingDelta <- tapply(X = wbal$BergingDelta, INDEX = wbal$plot, FUN = mean)
MeanA_BalDev <- tapply(X = wbal$BalDev, INDEX = wbal$plot, FUN = mean)
watbalYr <- data.frame(Plot=MeanA_Plotnr,Neerslag=MeanA_Neerslag,Irrigatie=MeanA_Irrigatie,
                     Infiltratie=MeanA_Infiltratie,Kwel=MeanA_Kwel,VerdampingGewas=MeanA_VerdampingGewas,
                     VerdampingInterc=MeanA_VerdampingInterc,VerdampingBodem=MeanA_VerdampingBodem,
                     Runoff=MeanA_Runoff,Drainage=MeanA_Drainage,
                     Wegzijging=MeanA_Wegzijging,BergingDelta=MeanA_BergingDelta,BalDev=MeanA_BalDev)
filnam = "./res/WatBal_plot.csv"
write.table(x = watbalYr, file = filnam , sep = ",", quote = FALSE,
              row.names = FALSE, col.names = TRUE, )

#  Grondwateraanvulling per plot (arithmetic average in mm water) aanvoer=pos, afvoer = neg
wbal$GrondwaterAanvoer <- -1*( wbal$Drainage + wbal$Wegzijging + wbal$Infiltratie + wbal$Kwel)
MeanA_Plotnr    <- tapply(X = wbal$plot, INDEX = wbal$plot, FUN = mean)
MeanA_GwAanvoer <- tapply(X = wbal$GrondwaterAanvoer, INDEX = wbal$plot, FUN = mean)
GwAanvoer <- data.frame(Plot=MeanA_Plotnr,GwAanvoer_mm=MeanA_GwAanvoer)
# zonder mv-drainage :  zit niet altijd in systeem 5   !!!! ??
#wbal$GrondwaterAanvoerx <- wbal$Drainage + wbal$Wegzijging - wbal$Infiltratie - wbal$Kwel - wb$fldrou5
#GwAanvoerx <- tapply(X = wbal$GrondwaterAanvoer, INDEX = wbal$plot, FUN = mean)
filnam = "./res/GrondwaterAanvoer.csv"
write.table(x = GwAanvoer, file = filnam , sep = ",", quote = FALSE,
              row.names = FALSE, col.names = TRUE, )


#  weighted average in mm water, using   key with areas (m2)
#filekenm <- "./ArableLandUsePlotken20110926.csv - complete.txt"
#keyplot <- read.csv(file = filekenm, header = TRUE, as.is = TRUE, skip = 0, sep = ",")
#keyplot <- subset(keyplot,keyplot$Plot %in%  watbalYr$Plot )
#keyplot$AreaM2 <- keyplot$OPPHA * 10000.0
#
#MeanW_Neerslag <- weighted.mean(x = MeanA_Neerslag, w = keyplot$AreaM2, na.rm = TRUE)
#MeanW_Irrigatie <- weighted.mean(x = MeanA_Irrigatie, w = keyplot$AreaM2, na.rm = TRUE)
#MeanW_Infiltratie <- weighted.mean(x = MeanA_Infiltratie, w = keyplot$AreaM2, na.rm = TRUE)
#MeanW_Kwel <- weighted.mean(x = MeanA_Kwel, w = keyplot$AreaM2, na.rm = TRUE)
#MeanW_VerdampingGewas <- weighted.mean(x = MeanA_VerdampingGewas, w = keyplot$AreaM2, na.rm = TRUE)
#MeanW_VerdampingInterc <- weighted.mean(x = MeanA_VerdampingInterc, w = keyplot$AreaM2, na.rm = TRUE)
#MeanW_VerdampingBodem <- weighted.mean(x = MeanA_VerdampingBodem, w = keyplot$AreaM2, na.rm = TRUE)
#MeanW_Runoff <- weighted.mean(x = MeanA_Runoff, w = keyplot$AreaM2, na.rm = TRUE)
#MeanW_Drainage <- weighted.mean(x = MeanA_Drainage, w = keyplot$AreaM2, na.rm = TRUE)
#MeanW_Wegzijging <- weighted.mean(x = MeanA_Wegzijging, w = keyplot$AreaM2, na.rm = TRUE)
#MeanW_BergingDelta <- weighted.mean(x = MeanA_BergingDelta, w = keyplot$AreaM2, na.rm = TRUE)
#MeanW_BalDev <- weighted.mean(x = MeanA_BalDev, w = keyplot$AreaM2, na.rm = TRUE)
#watbal <- data.frame(Neerslag=MeanW_Neerslag,Irrigatie=MeanW_Irrigatie,
#                     Infiltratie=MeanW_Infiltratie,Kwel=MeanW_Kwel,
#                     VerdampingGewas=MeanW_VerdampingGewas,VerdampingInterc=MeanW_VerdampingInterc,
#                     VerdampingBodem=MeanW_VerdampingBodem,
#                     Runoff=MeanW_Runoff,Drainage=MeanW_Drainage,
#                     Wegzijging=MeanW_Wegzijging,BergingDelta=MeanW_BergingDelta,BalDev=MeanW_BalDev,
#                     AantalPlots=length(unique(wb$ipl)))
#filnam = "./res/WatBal_weightedmean.csv"
#write.table(x = t(watbal), file = filnam , sep = ",", quote = FALSE,
#              row.names = TRUE, col.names = FALSE, )
#