# setwd("d:\\Kroes\\Prive\\Promotie\\artikel_VertDiscr_Kroes_Supit_etal\\test\\case3_grass_ave\\postproc")
# setwd("d:\\Kroes\\Prive\\artikel_CapRiseRecirc_Kroes_etal\\test_swap401_swsophy1\\case3_grass_ave\\postproc")

cat("work directory = ",getwd())
dir.create("./res")
# -- ranges
ymaxWP = 10
ymaxWFP = 1000

# -- read bofek2012 profielen and key from 21PAWN-profiles to dominant bofekprofile
bofek  <- read.csv(file = "../../cases_sophy_star_crp/BOFEK2012_profielen_20150622.csv", header = TRUE, as.is = TRUE, skip = 0)
bofek$NamBfk <- NA
bofek$NamBfk[bofek$BOFEK2012 %in% 101:110] <- "Peat"
bofek$NamBfk[bofek$BOFEK2012 %in% 201:206] <- "PeatMoor"
bofek$NamBfk[bofek$BOFEK2012 %in% 301:327] <- "Sand"
bofek$NamBfk[bofek$BOFEK2012 %in% 401:422] <- "Clay"
bofek$NamBfk[bofek$BOFEK2012 %in% 501:507] <- "Loam"

# groundwater table period 1971-2015
filnam <- list.files("./gxg",pattern=".gg.csv",full.names = TRUE)
gxg    <- read.csv(file = filnam[1], skip = 0)
plotnr <- as.integer(unlist(strsplit(filnam[1],"\\."))[4])
gxg[,1] <- plotnr
gxg$Namebofek2012 <- bofek$NamBfk[bofek$BOFEKnr==plotnr][1]
gxg$Unitbofek2012 <- bofek$BOFEK2012[bofek$BOFEKnr==plotnr][1]
gg <- gxg
for (i in (2:length(filnam)))  {
    gxg    <- read.csv(file = filnam[i], skip = 0)
    plotnr <- as.integer(unlist(strsplit(filnam[i],"\\."))[4])
    gxg[,1] <- plotnr
    gxg$Namebofek2012 <- bofek$NamBfk[bofek$BOFEKnr==plotnr][1]
    gxg$Unitbofek2012 <- bofek$BOFEK2012[bofek$BOFEKnr==plotnr][1]
    gg <- rbind(gg,gxg)
}
write.table(x = gg, file="./res/RdSwapResultGg.csv", row.names=FALSE, sep=",") 
# gt
png(file = "./res/RdSwapResultGt.png", width = 1024, height = 1024, pointsize = 22, bg = "white")
par(mfrow = c(2, 1),mar = c(bottom = 2, left = 4, top = 2, right = 2))
main <- paste("Gt (-)")
xlab <- paste("Soil")
ylab <- paste("Gt (-)")
# plot(x=stats$year, y=stats$Yact,main=c(main),xlab=xlab,ylab=ylab ,cex.sub = 0.75)
boxplot(Gtnr_extended ~ ipl,data=gg, ylim=c(0,7),xlab=xlab,ylab=ylab, 
	cex.lab=1.5,cex.axis=1.5,cex.amain=1.5)
boxplot(Gtnr_extended ~ Namebofek2012,data=gg,ylim=c(0,7),xlab=xlab,ylab=ylab,
	cex.lab=1.5,cex.axis=1.5,cex.amain=1.5)
dev.off()
# gxg
png(file = "./res/RdSwapResultGxg.png", width = 1024, height = 1024, pointsize = 22, bg = "white")
main <- paste("meanHigh =",round(mean(gg$ghg),1),"meanLow =",round(mean(gg$glg),1)," (cm-soil)")
xlab <- paste("Soil Number")
ylab <- paste("groundwater (cm-soil)")
plot(x=gg$ipl, y=gg$ghg,main=c(main),ylim=c(-250,0), col="blue",type="p", xlab=xlab,ylab=ylab,
	cex.lab=1.5,cex.axis=1.5,cex.main=1.5)
points(x=gg$ipl, y=gg$glg,col="red", pch=19, cex.sub = 0.75)
legend("bottomleft",legend=c("meanHigh","meanLow"), 
			cex=0.9, pch=c(1,19),col=c("blue","red") )
dev.off()
