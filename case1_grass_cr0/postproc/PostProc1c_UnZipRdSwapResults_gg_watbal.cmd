@echo off
rem ----------
rem  unzip swap results from zip files
md gxg
md watbal
rem 
for %%v  in (..\results\*.zip)  do unzip  %%v -n result.run.*.gg.csv       -d  .\gxg
for %%v  in (..\results\*.zip)  do unzip  %%v -n result.run.*.watbal.csv   -d  .\watbal
rem ----------

rem pause


call  PostProc1d_alldepths.cmd

