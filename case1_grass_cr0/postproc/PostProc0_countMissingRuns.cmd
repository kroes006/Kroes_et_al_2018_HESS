@echo off
rem ----------
rem echo zipfil > zipfiles.csv
rem dir/b *.zip >>zipfiles.csv
rem echo 
rem  unzip swap ok and log files from zip files
md oklog
rem 
for %%v  in (..\results\*.zip)  do unzip  %%v -n result.run.*.swap.ok    -d  .\oklog
rem for %%v  in (..\results\*.zip)  do unzip  %%v -n result.run.*.swap.log   -d  .\oklog
rem ----------
rem pause
R  --no-restore --no-save  < PostProc0_countmissingruns.R  > PostProc0_countmissingruns.log
exit