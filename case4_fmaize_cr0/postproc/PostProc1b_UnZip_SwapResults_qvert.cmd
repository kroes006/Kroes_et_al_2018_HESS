@echo off
rem ----------
rem  unzip swap results from zip files
md qvert
rem 
for %%v  in (..\results\*.zip)  do unzip  %%v -n result.run.*.swap.crz  -d  .\qvert
rem ----------

call PostProc1c_UnZipRdSwapResults_gg_watbal.cmd

rem pause
