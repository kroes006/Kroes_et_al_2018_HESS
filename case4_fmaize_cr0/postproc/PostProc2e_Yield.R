# setwd("d:\\Kroes\\Prive\\Promotie\\artikel_VertDiscr_Kroes_Supit_etal\\test\\cases_sophy_star_maize_Level1_50cm\\postproc")

#plotkenm <- read.csv(file="./ArableLandUsePlotken20110926.csv - complete.txt"
croptype <- "fmaize"     # as.character(subset(plotkenm,Plot==plotnr)$landuse)

cat("work directory = ",getwd())
dir.create("./res")

# crop results period 1971-1985
filnams1 <- list.files("./Yield",pattern=".swap.crp$",full.names = TRUE)
nplots   <- length(filnams1)

# of each crop-file:  read crop-results
for (iplot in (1:length(filnams1)))  {
	# iplot <- 1
	plotnr <- as.integer(unlist(strsplit(filnams1[iplot],"\\."))[4])
	crp <- read.csv(file = filnams1[iplot], skip = 7)
	#	determine yield of each year
	crp$Date <- as.Date(crp$Date)
	crp$year = 1900 + as.POSIXlt(crp$Date)$year
	yrs     <- unique(crp$year)
	nrofyrs <- length(yrs)
	simyr1  <- data.frame(ipl=rep(plotnr,nrofyrs),year=yrs,yield=rep(NA,nrofyrs),yieldpot=rep(NA,nrofyrs))
	for (iyr in (1:nrofyrs)) {
		# iyr<-1
		tmp <- subset(crp,year==yrs[iyr])
		if(croptype=="fmaize") { 
			if(sum(tmp$CWDM,na.rm=TRUE)>0) {
				simyr1$yield[iyr]    <- max(tmp$CWDM,na.rm=TRUE) 
				simyr1$yieldpot[iyr] <- max(tmp$CPWDM,na.rm=TRUE) }
		} else {
			if(sum(tmp$CWSO,na.rm=TRUE)>0) {
				simyr1$yield[iyr]    <- max(tmp$CWSO,na.rm=TRUE) 
				simyr1$yieldpot[iyr] <- max(tmp$CPWSO,na.rm=TRUE) }
		}
	}
	if(iplot==1) {simyr <- simyr1}
	if(iplot>1)  {simyr <- rbind(simyr,simyr1)}
}
write.table(x = simyr, file="./res/yields.csv", row.names=FALSE, sep=",") 

