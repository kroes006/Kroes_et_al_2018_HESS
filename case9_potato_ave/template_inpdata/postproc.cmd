copy   hupsel_swap.log  result.run.%1.swap.log
copy   swap.ok          result.run.%1.swap.ok
copy   result.crp       result.run.%1.swap.crp
copy   result.crz       result.run.%1.swap.crz
copy   result.str       result.run.%1.swap.str
rem   ----- waterbalance
copy   result.bun  result.1.bun
..\..\bin\RdSwapResultv12.exe  .\RdSwapResultv12.log  .\RdSwapResultv12.ini  .\plotnrs.csv.txt   .\   .\  
copy   RdSwapResultGg.csv  result.run.%1.gg.csv
copy   RdSwapResultYr.csv  result.run.%1.watbal.csv
rem   ----- save result
zip   result.run.%1.zip  result.run.%1.*
copy  result.run.%1.zip  ..\results\result.run.%1.zip 
rem pause
