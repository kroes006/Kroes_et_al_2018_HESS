# set working directory wd; all other dirs are relative to the wd
# setwd("d:/Kroes/Prive/artikel_VertDiscr_Kroes_Supit_etal/test/case1_grass_cr0")
# ---- general settings
# -- output-switches   on/off
swsophyMVG <- "off"
# swsophyMVG <- "on"

# -- load packages
# import/source tools
# source("../MyReadSwp.R")
wd <- getwd()
cat("working directory = ",wd, "\n")

# -- read numerical params to be entered in swp-file
filnam = "./Convert_inpSwp_crop.txt"
nums <- readLines(con = filnam) 

#read cases
dircase    <- read.csv(file="./runtestswap.csv.txt",header = TRUE, as.is = TRUE, skip = 0, sep = ",")
nsim <- length(dircase$case)

# run and analyse each case
for ( isim in (1:nsim) ) {
	# isim <- 1
	fileswp <- paste("../cases_sophy_star_crp/template_inpswp_BOFEK2012_ref/", dircase$case[isim], sep="")
	swpdata <- readLines(con = fileswp)

	# extend sim-period
	irec    <- grep(pattern = "TEND    = 31-dec-2013", fixed = TRUE, x = swpdata)
	swpdata[irec] <- gsub(pattern = "TEND    = 31-dec-2013",replacement = "TEND    = 31-dec-2015",x = swpdata[irec])

    # suppress messages
	irec    <- grep(pattern = " SWSCRE    = 1", fixed = TRUE, x = swpdata)
	swpdata[irec] <- gsub(pattern = " SWSCRE    = 1",replacement = " SWSCRE    = 0",x = swpdata[irec])

    # reduce evaporation by bare soil
	irec    <- grep(pattern = " CFBS   = 1.0", fixed = TRUE, x = swpdata)
	swpdata[irec] <- gsub(pattern = " CFBS   = 1.0",replacement = " CFBS   = 0.5",x = swpdata[irec])

    # change bbc:  from closed to free drainage and suppress drainage
	irec    <- grep(pattern = " SWBOTB = 6", fixed = TRUE, x = swpdata)
	swpdata[irec] <- gsub(pattern = " SWBOTB = 6",replacement = " SWBOTB = 7",x = swpdata[irec])
	irec    <- grep(pattern = " SWDRA = 1", fixed = TRUE, x = swpdata)
	swpdata[irec] <- gsub(pattern = " SWDRA = 1",replacement = " SWDRA = 0",x = swpdata[irec])
#	irec    <- grep(pattern = "01-jan-1981     -95.0", fixed = TRUE, x = swpdata)
#	swpdata[irec] <- gsub(pattern = "01-jan-1981     -95.0",replacement = "01-jan-1971     -50.0",x = swpdata[irec])
#	irec    <- grep(pattern = "31-dec-1983     -95.0", fixed = TRUE, x = swpdata)
#	swpdata[irec] <- gsub(pattern = "31-dec-1983     -95.0",replacement = "31-dec-2015     -50.0",x = swpdata[irec])

    # change initial conditions if free drainage (swbotb=7)
	irec    <- grep(pattern = " SWINCO = 2 !", fixed = TRUE, x = swpdata)
	swpdata[irec] <- gsub(pattern = " SWINCO = 2 !",replacement = " SWINCO = 1 !",x = swpdata[irec])
	irec    <- grep(pattern = "   -0.5     -92.831", fixed = TRUE, x = swpdata)
	swpdata[irec] <- gsub(pattern = "   -0.5     -92.831",replacement = "   -0.5     -100.0",x = swpdata[irec])
	irec    <- grep(pattern = " -195.0      99.591", fixed = TRUE, x = swpdata)
	swpdata[irec] <- gsub(pattern = " -195.0      99.591",replacement = " -195.0     -100.0",x = swpdata[irec])

	# ETref
	#	irec1    <- grep(pattern = "  SWETR  =  1", fixed = TRUE, x = swpdatanew)
	#    swpdatanew[irec1] <- paste( "  SWETR  =  0           ! Switch, use reference ET values of meteo file [Y=1, N=0]")

    # sophy as table (default) or as MVG 
	if(swsophyMVG=="on") {
		irec    <- grep(pattern = " SWSOPHY = 1", fixed = TRUE, x = swpdata)
		swpdata[irec]  <- gsub(pattern = " SWSOPHY = 1",replacement = " SWSOPHY = 0",x = swpdata[irec])
	}

	#  implicit solution to enable/optimize switch to minimize cap rise below rootzone
#	irec    <- grep(pattern = "SWkImpl = 0", fixed = TRUE, x = swpdata)
#	swpdata[irec] <- gsub(pattern = "SWkImpl = 0",replacement = "SWkImpl = 1",x = swpdata[irec])

	swpdata[(length(swpdata)+1)] <- paste("SWCAPRISEOUTPUT = .true.")

	#   crop calendar
	irec1    <- grep(pattern = " CROPSTART      CROPEND ", fixed = TRUE, x = swpdata)
	irec2    <- irec1+44
        swpdatanew <- c(swpdata[1:(irec1-1)], nums, swpdata[(irec2+1):length(swpdata)])

	fileswp <- paste("./template_inpswp/", dircase$case[isim], sep="")
	writeLines(text = swpdatanew, con = paste("./", fileswp, sep = ""))
}
