**********************************************************************************
* Bottom boundary condition

* STONE plot nr  2245

* If SWBBCFILE = 0, select one of the following options:
             ! 1  Prescribe groundwater level
             ! 2  Prescribe bottom flux
             ! 3  Calculate bottom flux from hydraulic head of deep aquifer
             ! 4  Calculate bottom flux as function of groundwater level
             ! 5  Prescribe soil water pressure head of bottom compartment
             ! 6  Bottom flux equals zero
             ! 7  Free drainage of soil profile
             ! 8  Free outflow at soil-air interface

 SWBOTB = 2  ! Switch for bottom boundary [1..8,-,I]

* Options 1,2,3,4,and 5 require additional data as specified below!
**********************************************************************************


**********************************************************************************
* SWBOTB = 1  Prescribe groundwater level

* specify DATE [dd-mmm-yyyy] and groundwater level [cm, -10000..1000, R] 

        DATE1    GWLEVEL         ! (max. MABBC records)
  01-jan-1981     -95.0
  31-dec-1983     -95.0
* End of table                                                     
**********************************************************************************


**********************************************************************************
* SWBOTB = 2   Prescribe bottom flux

* Specify whether a sine or a table are used to prescribe the bottom flux:
  SW2    = 1      ! Sine function = 1,  table = 2

* In case of sine function (SW2 = 1), specify:
  SINAVE =  6.2300e-02  ! Average value of bottom flux, [-10..10 cm/d, R, + = upwards]
  SINAMP =  5.3400e-02  ! Amplitude of bottom flux sine function, [-10..10 cm/d, R]
  SINMAX =  2.7000e+02  ! Time of the year with maximum bottom flux, [1..366 d, R]  

* In case of table (SW2 = 2), specify date [dd-mmm-yyyy] and bottom flux QBOT2
* [-100..100 cm/d, R, positive = upwards]:

        DATE2     QBOT2           ! (maximum MABBC records)
  01-jan-1980       0.1
  30-jun-1980       0.2
  23-dec-1980      0.15
* End of table
**********************************************************************************


**********************************************************************************
* SWBOTB = 3    Calculate bottom flux from hydraulic head in deep aquifer

* Switch to suppress addition of vertical resistance between bottom of model and 
* groundwater level 0 = default, 1 = suppress
  SWBOTB3RESVERT = 0 ! Switch to suppress additional resistance [0,1, I]

* Switch for implicit solution with bottom flux: 0 = explicit, 1 = implicit
* Do not use SWBOTB3IMPL = 1 in combination with SHAPE < 1.0
  SWBOTB3IMPL = 0   ! Switch for kmean in implicit numerical solution with, [0,1, I]

* Specify:
  SHAPE  =   1.0        ! Shape factor to derive average groundwater level, [0..1 -, R]
  HDRAIN =  -25.0       ! Mean drain base to correct for average groundwater level, [-10000..0 cm, R]
  RIMLAY =   5.3800e+03 ! Vertical resistance of aquitard, [0..10000 d, R]

* Specify whether a sine or a table are used to prescribe hydraulic head of deep aquifer:
  SW3    = 1       ! 1 = Sine function,  2 = table 

* In case of sine function (SW3  = 1), specify:
  AQAVE  = -1.3800e+02   ! Average hydraulic head in underlaying aquifer, [-10000..1000 cm, R] 
  AQAMP  =   0.0         ! Amplitude hydraulic head sinus wave, [0..1000 cm, R]
  AQTMAX =  30.0  ! First time of the year with maximum hydraulic head, [1..366 d, R]
  AQPER  =  365.0  ! Period hydraulic head sinus wave, [1..366 d, I]

* In case of table (SW3  = 2), specify date [dd-mmm-yyyy] and average hydraulic head 
* HAQUIF in underlaying aquifer [-10000..1000 cm, R]:

        DATE3    HAQUIF           ! (maximum MABBC records)
  01-jan-1980     -95.0
  30-jun-1980    -110.0
  23-dec-1980     -70.0
* End of table

* An extra groundwater flux can be specified which is added to above specified flux
  SW4   = 0        ! 0 = no extra flux, 1 = include extra flux

* If SW4 = 1, specify date [dd-mmm-yyyy] and bottom flux QBOT4 [-100..100 cm/d, R, 
* positive = upwards]:

        DATE4     QBOT4           ! (maximum MABBC records)
  01-jan-1980       1.0
  30-jun-1980     -0.15
  23-dec-1980       1.2
* End of table
**********************************************************************************


**********************************************************************************
* SWBOTB = 4     Calculate bottom flux as function of groundwater level

* Specify whether an exponential relation or a table is used to determine qbot from groundwaterlevel:
  SWQHBOT = 2       ! 1 = exponential relation,  2 = table 
 
* In case of an exponential relation (SWQHBOT  = 1),
* Specify coefficients of relation qbot = A exp (B*abs(groundwater level))
  COFQHA =  0.1  ! Coefficient A, [-100..100 cm/d, R]
  COFQHB =  0.5  ! Coefficient B  [-1..1 /cm, R]

* Hidden option:
* Specify coefficients of relation qbot = A exp(B*abs(groundwater level)) + C
* Oostelijk N-Brabant rug:  A = -0.133 en  B= -0.01 en C= -0.03
*  COFQHC = -0.03    ! Coefficient C  [-10..10 cm/d, R]

* In case of a table (SWQHBOT  = 2),
* Specify groundwaterlevel Htab [-10000..1000, cm, R]  and bottom flux QTAB [-100..100 cm/d, R]
* Htab is negative below the soil surface, Qtab is negative when flux is downward.
  HTAB   QTAB
  -0.1   -0.35
  -70.0  -0.05
 -125.0  -0.01
**********************************************************************************


**********************************************************************************
* SWBOTB = 5     Prescribe soil water pressure head of bottom compartment
 
* Specify DATE [dd-mmm-yyyy] and bottom compartment pressure head HBOT5 
* [-1.d10..1000 cm, R]: 

        DATE5     HBOT5           ! (maximum MABBC records)
  01-jan-1980     -95.0
  30-jun-1980    -110.0
  23-dec-1980     -70.0
* End of table
**********************************************************************************
