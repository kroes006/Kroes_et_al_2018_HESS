@echo off
rem ----------
rem  unzip swap results from zip files
md yield
rem 
for %%v  in (..\results\*.zip)  do unzip  %%v -n result.run.*.swap.crp   -d  .\yield
rem ----------

call PostProc1b_UnZip_SwapResults_qvert.cmd

rem pause
