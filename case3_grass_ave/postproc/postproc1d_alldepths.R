# set working directory wd; all other dirs are relative to the wd 
# setwd("d:\\Kroes\\Prive\\artikel_VertDiscr_Kroes_Supit_etal\\test\\case9_potato_ave\\postproc")
# setwd("d:\\Kroes\\Prive\\artikel_CapRiseRecirc_Kroes_etal\\test_swap401_swsophy1\\case3_grass_ave\\postproc")

wd <- getwd()
cat("working directory = ",wd, "\n")
# ---- general settings
# -- load packages
# library(RSWAP)
# library(xtable)
library(ggplot2)
# function to read header of observation files
readHeader <- 
function(filename) {
    grep(
        pattern = "^#", 
        x = readLines(con = filename, warn = FALSE), 
        value = TRUE
    )
}
# -- output-switches   on/off
ymaxWP = 10
ymaxWFP = 1000
Yactmin = 0
Yactmax = 20000
Tactmin = 0
Tactmax = 600
qvupmin = 0
qvupmax = 250

# -- read bofek2012 profielen and key from 21PAWN-profiles to dominant bofekprofile
bofek  <- read.csv(file = "../../cases_sophy_star_crp/BOFEK2012_profielen_20150622.csv", header = TRUE, as.is = TRUE, skip = 0)
bofek$NamBfk <- NA
bofek$NamBfk[bofek$BOFEK2012 %in% 101:110] <- "Peat"
bofek$NamBfk[bofek$BOFEK2012 %in% 201:206] <- "PeatMoor"
bofek$NamBfk[bofek$BOFEK2012 %in% 301:327] <- "Sand"
bofek$NamBfk[bofek$BOFEK2012 %in% 401:422] <- "Clay"
bofek$NamBfk[bofek$BOFEK2012 %in% 501:507] <- "Loam"

#read cases
balFiles <- list.files(path = "./watbal", pattern = "watbal.csv", full.names = TRUE)
gxgFiles <- list.files(path = "./gxg", pattern = "gg.csv", full.names = TRUE)
crpFiles <- list.files(path = "./yield", pattern = "swap.crp", full.names = TRUE)
qvcrzFiles <- list.files(path = "./qvert", pattern = "swap.crz", full.names = TRUE)
nsim <- (length(balFiles)+length(gxgFiles)+length(crpFiles)+length(qvcrzFiles)) / 4
if (nsim!=72)  { cat("Error, nr of RdSwapResultYr = ",nsim, "\n") }

# read nrofyears
filefrom <- paste(balFiles[1])
wba      <- read.csv(file = filefrom, header = TRUE, as.is = TRUE) 
yrs      <- unique(wba$yr)
nrofyrs  <- length(yrs)

# -- read crop calender
# filefrom = "../cropcalender.txt"
filefrom = "../Convert_inpSwp_crop.txt"
cropcal <- read.table(file = filefrom, header = TRUE) 
tmp <- as.character(cropcal$CROPSTART)
cropcal$DateSowing <- as.Date(tmp, format="%d-%b-%Y")
tmp <- as.character(cropcal$CROPEND)
cropcal$DateHarvest <- as.Date(tmp, format="%d-%b-%Y")

# --- initialise stats
ETact <- rep(NA,(nsim*nrofyrs))
stats <- data.frame(case=ETact,Namebofek2012=ETact,year=ETact,Unitbofek2012=ETact,
	ETact=ETact,ETpot=ETact,Tact=ETact,Tpot=ETact,
	Yact=ETact,Ypot=ETact,qvcuprz=ETact,qvcdnrz=ETact,
	ghg=ETact,glg=ETact,gtnrext=ETact,gt=ETact,
	WP_Tact=ETact,WFP_Tact=ETact,WP_ETact=ETact,WFP_ETact=ETact,
	qvbotupyr=ETact,qvbotdnyr=ETact)   #,qvbotupcrp=ETact,qvbotdncrp=ETact)

# run and analyse each case
for ( isim in (1:nsim) ) {
	# isim <- 1
	
	rec1 <- isim + (nrofyrs*(isim-1))
    if(isim>1) {rec1 = rec1 - isim+1}
	rec2 <- (nrofyrs*(isim))

	stats$Namebofek2012[rec1:rec2]    <- bofek$NamBfk[bofek$BOFEKnr==isim][1]
	stats$Unitbofek2012[rec1:rec2]    <- bofek$BOFEK2012[bofek$BOFEKnr==isim][1]

	# read wba
	filefrom <- paste(balFiles[isim])
	wba      <- read.csv(file = filefrom, header = TRUE, as.is = TRUE) 
	stats$case[rec1:rec2]  <- isim
	stats$year[rec1:rec2]  <- wba$yr
	stats$ETact[rec1:rec2] <- -1*(wba$evicpr+wba$evicir+wba$evso+wba$evsubl+wba$evpn+wba$flev)
	stats$ETpot[rec1:rec2] <- -1*(wba$evicpr+wba$evicir+wba$evsoma+wba$evtrma)
	stats$Tact[rec1:rec2]  <- -1*(wba$flev)
	stats$Tpot[rec1:rec2]  <- -1*(wba$evtrma)
	stats$qvbotupyr[rec1:rec2]  <- wba$flbtin
	stats$qvbotdnyr[rec1:rec2]  <- -1*(wba$flbtou)

	# read crp
	filefrom <- paste(crpFiles[isim])
	crp      <- read.csv(file = filefrom, header = TRUE, as.is = TRUE, skip = 7) 
	crp$year <- as.POSIXlt(as.Date(crp$Date))$year+1900
	Ypot <- rep(NA,nrofyrs)
	Yact <- rep(NA,nrofyrs)
	for (iyr in 1:nrofyrs) {
		# iyr <- 1
		yr <- yrs[iyr]
		tmp  <- subset(crp,year==yr)
		if(unique(cropcal$CROPNAME)=="Grass") {
			tmp$GrassAct <- tmp$MOWDM+tmp$GRAZDM
			Yact[iyr] <- max(tmp$GrassAct,na.rm=TRUE)
			tmp$GrassPot <- tmp$PMOWDM+tmp$PGRAZDM
			Ypot[iyr] <- max(tmp$GrassPot,na.rm=TRUE)
		}
		if(unique(cropcal$CROPNAME)=="Maize") {
			Yact[iyr] <- max(tmp$CWDM,na.rm=TRUE)
			Ypot[iyr] <- max(tmp$CPWDM,na.rm=TRUE)
		}
		if(unique(cropcal$CROPNAME)=="Potato") {
			Yact[iyr] <- max(tmp$CWSO,na.rm=TRUE)
			Ypot[iyr] <- max(tmp$CPWSO,na.rm=TRUE)
		}
	}
	stats$Yact[rec1:rec2] <- Yact
	stats$Ypot[rec1:rec2] <- Ypot	

	# read qvc capillary rise (upward flux = pos) during growth period 
#	depths <- rz
	filefrom <- paste(qvcrzFiles[isim])
	qvc      <- read.csv(file = filefrom, header = TRUE, as.is = TRUE, skip = 6) 
	qvc$Date <- as.Date(qvc$date)
	qvc$year <- as.POSIXlt(as.Date(qvc$Date,format="%Y-%m-%d"))$year+1900
	qvcyrup <- rep(NA,nrofyrs)
	qvcyrdn <- rep(NA,nrofyrs)
	for (iyr in 1:nrofyrs) {
		# iyr <- 1
		yr <- yrs[iyr]
		tmp  <- subset(qvc, year==yr )
		tmpup  <- subset(tmp, (tmp[,5]>0) )
		tmpdn  <- subset(tmp, (tmp[,5]<0) )
		qvcyrup[iyr] <- sum(10*tmpup[,5])
		qvcyrdn[iyr] <- sum(-10*tmpdn[,5])
#		tmp  <- subset(qvc, (qvc$Date>=cropcal$DateSowing[iyr] & qvc$Date<=cropcal$DateHarvest[iyr]) )
	}
	stats$qvcuprz[rec1:rec2] <- qvcyrup    # in mm/cropping period
	stats$qvcdnrz[rec1:rec2] <- qvcyrdn	   # in mm/cropping period

	# read gxg
	filefrom <- paste(gxgFiles[isim])
	gxg      <- read.csv(file = filefrom, header = TRUE, as.is = TRUE) 
	stats$ghg[rec1:rec2] <- gxg$ghg
	stats$glg[rec1:rec2] <- gxg$ghg
	stats$gtnrext[rec1:rec2]  <- gxg$Gtnr_extended
	stats$gt[rec1:rec2]  <- gxg$gt

}
stats$Trel      <- stats$Tact/stats$Tpot
stats$Yrel      <- stats$Yact/stats$Ypot
stats$YredPerc  <- 100*(stats$Ypot-stats$Yact)/stats$Ypot
stats$Yrelexp   <- stats$Yact/(0.8*stats$Ypot)
stats$WP_Tact   <- stats$Yact/stats$Tact / 10    # g/ltr or kg/m3
stats$WFP_Tact  <- 1 / stats$WP_Tact*1000             # ltr/kg
stats$WP_ETact  <- stats$Yact/stats$ETact / 10    # g/ltr or kg/m3
stats$WFP_ETact <- 1 / stats$WP_ETact*1000             # ltr/kg

# save results of stats
dir.create("./res")
filnam <- paste("./res/Stats.csv.txt",sep="")
write.table(x = stats, file = filnam , sep = ",", quote = FALSE,
              row.names = FALSE, col.names = TRUE)

# Yact
png(file = "./res/Bofek_Yact.png", width = 1024, height = 1024, pointsize = 22, bg = "white")
par(mfrow = c(2, 1),mar = c(bottom = 2, left = 5, top = 2, right = 2))
main <- paste("Yact (kg/ha DM)")
xlab <- paste("Time")
par(adj = 0)
ylab <- paste("Yact (kg/ha DM)")
# plot(x=stats$year, y=stats$Yact,main=c(main),xlab=xlab,ylab=ylab ,cex.sub = 0.75)
boxplot(Yact ~ year, data=stats, ylim=c(Yactmin,Yactmax), xlab=xlab,ylab="", main=main, las=1,
	cex.lab=1.5,cex.axis=1.5,cex.amain=1.5)
boxplot(Yact ~ Namebofek2012,data=stats, ylim=c(Yactmin,Yactmax),xlab=xlab, ylab="", las=1,
	cex.lab=1.5,cex.axis=1.4,cex.amain=1.5)
dev.off()
# prints
A 					<- boxplot(Yact ~ year, data=stats, plot=FALSE)
YactYear 			<- A$stats
colnames(YactYear)	<- A$names
rownames(YactYear)	<- c('min','lower quartile','median','upper quartile','max')
filnam <- paste("./res/Stats_YactYear.csv",sep="")
write.table(x = YactYear, file = filnam , sep = ",", quote = FALSE,
              row.names = TRUE, col.names = TRUE)
A 					<- boxplot(Yact ~ Namebofek2012, data=stats, plot=FALSE)
YactSoil 			<- A$stats
colnames(YactSoil)	<- A$names
rownames(YactSoil)	<- c('min','lower quartile','median','upper quartile','max')
filnam <- paste("./res/Stats_YactSoil.csv",sep="")
write.table(x = YactSoil, file = filnam , sep = ",", quote = FALSE,
              row.names = TRUE, col.names = TRUE)

# Tact
png(file = "./res/Bofek_Tact.png", width = 1024, height = 1024, pointsize = 22, bg = "white")
par(mfrow = c(2, 1),mar = c(bottom = 2, left = 4, top = 2, right = 2))
main <- paste("Tact (mm/yr)")
xlab <- paste("Time")
ylab <- paste("Tact (mm/yr)")
par(adj = 0)
boxplot(Tact ~ year, data=stats, ylim=c(Tactmin,Tactmax), xlab=xlab,ylab="", main=main, las=1)
boxplot(Tact ~ Namebofek2012,data=stats, ylim=c(Tactmin,Tactmax),xlab=xlab,ylab="", main=main, las=1)
dev.off()
# prints
A 					<- boxplot(Tact ~ year, data=stats, plot=FALSE)
TactYear 			<- A$stats
colnames(TactYear)	<- A$names
rownames(TactYear)	<- c('min','lower quartile','median','upper quartile','max')
filnam <- paste("./res/Stats_TactYear.csv",sep="")
write.table(x = TactYear, file = filnam , sep = ",", quote = FALSE,
              row.names = TRUE, col.names = TRUE)
A 					<- boxplot(Tact ~ Namebofek2012, data=stats, plot=FALSE)
TactSoil 			<- A$stats
colnames(TactSoil)	<- A$names
rownames(TactSoil)	<- c('min','lower quartile','median','upper quartile','max')
filnam <- paste("./res/Stats_TactSoil.csv",sep="")
write.table(x = TactSoil, file = filnam , sep = ",", quote = FALSE,
              row.names = TRUE, col.names = TRUE)

# ETact
png(file = "./res/Bofek_ETact.png", width = 1024, height = 1024, pointsize = 22, bg = "white")
par(mfrow = c(2, 1),mar = c(bottom = 2, left = 4, top = 2, right = 2))
main <- paste("ETact (mm/yr)")
xlab <- paste("Time")
ylab <- paste("ETact (mm/yr)")
boxplot(ETact ~ year, data=stats, ylim=c(Tactmin,Tactmax), xlab=xlab,ylab=ylab)
boxplot(ETact ~ Namebofek2012,data=stats, ylim=c(Tactmin,Tactmax),xlab=xlab,ylab=ylab)
dev.off()
# prints
A 					<- boxplot(ETact ~ year, data=stats, plot=FALSE)
ETactYear 			<- A$stats
colnames(ETactYear)	<- A$names
rownames(ETactYear)	<- c('min','lower quartile','median','upper quartile','max')
filnam <- paste("./res/Stats_ETactYear.csv",sep="")
write.table(x = ETactYear, file = filnam , sep = ",", quote = FALSE,
              row.names = TRUE, col.names = TRUE)
A 					<- boxplot(ETact ~ Namebofek2012, data=stats, plot=FALSE)
ETactSoil 			<- A$stats
colnames(ETactSoil)	<- A$names
rownames(ETactSoil)	<- c('min','lower quartile','median','upper quartile','max')
filnam <- paste("./res/Stats_ETactSoil.csv",sep="")
write.table(x = ETactSoil, file = filnam , sep = ",", quote = FALSE,
              row.names = TRUE, col.names = TRUE)

# YredPerc
png(file = "./res/Bofek_YredPerc.png", width = 1024, height = 1024, pointsize = 22, bg = "white")
par(mfrow = c(2, 1),mar = c(bottom = 2, left = 4, top = 2, right = 2))
main <- paste("Yred (%)")
xlab <- paste("Time")
ylab <- paste("Yred (%)")
boxplot(YredPerc ~ year, data=stats, ylim=c(0,100), xlab=xlab,ylab=ylab)
boxplot(YredPerc ~ Namebofek2012,data=stats, ylim=c(0,100),xlab=xlab,ylab=ylab)
dev.off()
# prints
A 					<- boxplot(YredPerc ~ year, data=stats, plot=FALSE)
YredYear 			<- A$stats
colnames(YredYear)	<- A$names
rownames(YredYear)	<- c('min','lower quartile','median','upper quartile','max')
filnam <- paste("./res/Stats_YredPercYear.csv",sep="")
write.table(x = YredYear, file = filnam , sep = ",", quote = FALSE,
              row.names = TRUE, col.names = TRUE)
A 					<- boxplot(YredPerc ~ Namebofek2012, data=stats, plot=FALSE)
YredSoil 			<- A$stats
colnames(YredSoil)	<- A$names
rownames(YredSoil)	<- c('min','lower quartile','median','upper quartile','max')
filnam <- paste("./res/Stats_YredSoil.csv",sep="")
write.table(x = YredSoil, file = filnam , sep = ",", quote = FALSE,
              row.names = TRUE, col.names = TRUE)

# CapRise at bottom RootZone
#png(file = "./res/Bofek_CapRiserz.png", width = 1024, height = 1024, pointsize = 22, bg = "white")
png(file = "./res/Bofek_qUpwardrz.png", width = 1024, height = 1024, pointsize = 22, bg = "white")
par(mfrow = c(2, 1),mar = c(bottom = 2, left = 4, top = 2, right = 2))
main <- paste("Upward flux across bottom RootZone (mm / crop season)")
par(adj = 0)
xlab <- paste("Time")
ylab <- paste("Upward flux (mm/crop season)")
boxplot((stats$qvcuprz) ~ year, data=stats, ylim=c(qvupmin,qvupmax), xlab=xlab,ylab="", main=main, las=1,
	cex.lab=1.5,cex.axis=1.5,cex.amain=1.5)
boxplot((stats$qvcuprz) ~ Namebofek2012,data=stats, ylim=c(qvupmin,qvupmax),xlab=xlab,ylab="", las=1,
	cex.lab=1.5,cex.axis=1.5,cex.amain=1.5)
dev.off()
# prints
A 					<- boxplot((-1*qvcuprz) ~ year, data=stats, plot=FALSE)
qvupYear 			<- A$stats
colnames(qvupYear)	<- A$names
rownames(qvupYear)	<- c('min','lower quartile','median','upper quartile','max')
filnam <- paste("./res/Stats_qvuprzYear.csv",sep="")
write.table(x = qvupYear, file = filnam , sep = ",", quote = FALSE,
              row.names = TRUE, col.names = TRUE)
A 					<- boxplot((-1*qvcuprz) ~ Namebofek2012, data=stats, plot=FALSE)
qvupSoil 			<- A$stats
colnames(qvupSoil)	<- A$names
rownames(qvupSoil)	<- c('min','lower quartile','median','upper quartile','max')
filnam <- paste("./res/Stats_qvuprzSoil.csv",sep="")
write.table(x = qvupSoil, file = filnam , sep = ",", quote = FALSE,
              row.names = TRUE, col.names = TRUE)

# downward leaching flux across bottom of profile
png(file = "./res/Bofek_leach_bot_yr.png", width = 1024, height = 1024, pointsize = 22, bg = "white")
par(mfrow = c(2, 1),mar = c(bottom = 2, left = 4, top = 2, right = 2))
main <- paste("Downward leaching across bottom Profile (mm/yr)")
xlab <- paste("Time")
ylab <- paste("Downward leaching bottom (mm/yr)")
boxplot((stats$qvbotdnyr) ~ year, data=stats, xlab=xlab,ylab=ylab)
boxplot((stats$qvbotdnyr) ~ Namebofek2012,data=stats, xlab=xlab,ylab=ylab)
dev.off()


# Yields:  sim vs obs  ---------------------------------------------
# read observed yields
filnam <- paste("../observed/Eurostat_NL_crop_greenMaize.csv",sep="")
header    <- readHeader(filnam)
obs1      <- read.csv(file = filnam, header = TRUE, strip.white = TRUE, skip = length(header)) 
# aggregate 
Yact_median <- tapply(stats$Yact,stats$year,median)
Yact_mean 	<- tapply(stats$Yact,stats$year,mean)
# plot sim and obs
png(file = "./res/Bofek_Yact_simobs.png", width = 1024, height = 768, pointsize = 22, bg = "white")
main <- paste("Yrel f Trel (-) gemiddelde 1971-2015")
xlim <- c(xmin=min(stats$year), xmax=max(stats$year))
#plot(as.numeric(names(Yact_median)),y=(0.001*Yact_median),xlab="", ylab="yield (ton/ha DM)", xlim=xlim,ylim=c(0,20),col="black")
plot(as.numeric(names(Yact_mean)),y=(0.001*Yact_mean),xlab="",ylab="yield (ton/ha DM)", xlim=xlim,ylim=c(0,20),col="black",lty=2)
lines (x=as.numeric(names(Yact_mean)),y=(0.001*Yact_mean), col="black")
points(x=obs1$year,y=obs1$yieldDM, col="red" , pch=19)
lines (x=obs1$year,y=obs1$yieldDM, lty=3, col="red")
legend("bottomleft",legend=c("simulated_mean","observed"), 
		pch=c(-1,19), lty=c(1,3),col=c("black","red"),lwd=c(1,1))
dev.off()

# Yields : plot using ggplot(stats)
png(file = "./res/Bofek_Yact_GGplots1.png", width = 1024, height = 768, pointsize = 22, bg = "white")
ggplot(data=stats, mapping=aes(x=year,y=Yact)) + geom_point() #  + stat_smooth()
# ggplot(data=stats, mapping=aes(x=factor(year),y=glg)) + geom_boxplot()
#	geom_point() +
dev.off()
# Yields : plot using boxplots
png(file = "./res/Bofek_Yact_GGplots2.png", width = 1024, height = 768, pointsize = 22, bg = "white")
ggplot(data=stats, mapping=aes(x=Namebofek2012,y=Yact)) + geom_point() #  + stat_smooth()
# ggplot(data=stats, mapping=aes(x=factor(year),y=glg)) + geom_boxplot()
#	geom_point() +
dev.off()

# plots -----------------------------------------------------------
#par(mfrow = c(2, 1),mar = c(bottom = 2, left = 4, top = 2, right = 2))
# Trel
png(file = "./res/Bofek_YrelTrel.png", width = 1024, height = 1024, pointsize = 22, bg = "white")
main <- paste("Yrel f Trel (-) gemiddelde 1971-2015")
xlab <- paste("Trel")
ylab <- paste("Yrel")
xymax <- 1.0   # max(stats$Trel,stats$Yrel)
plot(x=stats$Trel, y=stats$Yrel,xlim=c(0,xymax),ylim=c(0,xymax) ,
		main=c(main),xlab=xlab,ylab=ylab ,cex.sub = 0.75)
lines(x=c(0,xymax),y=c(0,xymax), lty = 2 )
dev.off()

png(file = "./res/Bofek_Trel_Yrel.png", width = 1024, height = 1024, pointsize = 22, bg = "white")
par(mfrow = c(2, 2),mar = c(bottom = 2, left = 4, top = 2, right = 2))
main <- paste("Trel (-), 1971-2015")
xlab <- paste("Bofek2012")
ylab <- paste("Trel")
xymax <- 1.0   # max(stats$Trel,stats$Yrel)
plot(x=stats$case, y=stats$Trel,xlim=c(0,72),ylim=c(0,xymax) ,
		main=c(main),xlab=xlab,ylab=ylab ,cex.sub = 0.75)
boxplot(stats$Trel,main="Trel (-)",ylim=c(0,1))
main <- paste("Yrel (-), 1971-2015")
xlab <- paste("Bofek2012")
ylab <- paste("Yrel")
xymax <- 1.0   # max(stats$Trel,stats$Yrel)
plot(x=stats$case, y=stats$Yrel,xlim=c(0,72),ylim=c(0,xymax) ,
		main=c(main),xlab=xlab,ylab=ylab ,cex.sub = 0.75)
boxplot(stats$Yrel,main="Yrel (-)",ylim=c(0,1))
dev.off()

png(file = "./res/Bofek_YrelvsTrel.png", width = 1024, height = 1024, pointsize = 22, bg = "white")
main <- paste("Yrel en Trel (-) periode 1971-2015")
xlab <- paste("Trel")
ylab <- paste("Yrel")
xymax <- 1.0   # max(stats$Trel,stats$Yrel)
plot(x=stats$Trel, y=stats$Yrel,xlim=c(0,xymax),ylim=c(0,xymax) ,
		main=c(main),xlab=xlab,ylab=ylab ,cex.sub = 0.75)
lines(x=c(0,xymax),y=c(0,xymax), lty = 2 )
dev.off()

# Yrel en Trel
png(file = "./res/Bofek_YrelTrel.png", width = 1024, height = 1024, pointsize = 22, bg = "white")
par(mfrow = c(2, 1),mar = c(bottom = 2, left = 4, top = 2, right = 2))
main <- paste("Yrel (-)")
xlab <- paste("Time")
ylab <- paste("Yrel")
xymax <- 1.0   # max(stats$Trel,stats$Yrel)
plot(x=stats$year, y=stats$Yrel, ylim=c(0,xymax) ,
		main=c(main),xlab=xlab,ylab=ylab ,cex.sub = 0.75)
main <- paste("Trel (-)")
xlab <- paste("Time")
ylab <- paste("Trel")
xymax <- 1.0   # max(stats$Trel,stats$Yrel)
plot(x=stats$year, y=stats$Trel, ylim=c(0,xymax) ,
		main=c(main),xlab=xlab,ylab=ylab ,cex.sub = 0.75)
dev.off()

# qvcup (mm/yr/crop period, neg=upward)
png(file = "./res/Bofek_caprise1.png", width = 1024, height = 1024, pointsize = 22, bg = "white")
plot(stats[,c(7:11)])
dev.off()

png(file = "./res/Bofek_caprise2.png", width = 1024, height = 1024, pointsize = 22, bg = "white")
main <- paste("Yact (kg/ha DM) as f(qvcuprz (mm/yr))")
xlab <- paste("qvcuprz")
ylab <- paste("Yact")
plot(x=stats$qvcuprz, y=stats$Yact ,
		main=c(main),xlab=xlab,ylab=ylab ,cex.sub = 0.75)
dev.off()

png(file = "./res/Bofek_caprise3.png", width = 1024, height = 1024, pointsize = 22, bg = "white")
main <- paste("Yact (kg/ha DM) as f(qvcdnrz (mm/yr))")
xlab <- paste("qvcdnrz")
ylab <- paste("Yact")
plot(x=stats$qvcdnrz, y=stats$Yact ,
		main=c(main),xlab=xlab,ylab=ylab ,cex.sub = 0.75)
dev.off()

# extra boxplots
png(file = "./res/Bofek_TY_boxplot.png", width = 1024, height = 768, pointsize = 22, bg = "white")
par(mfrow = c(2, 2),mar = c(bottom = 2, left = 4, top = 2, right = 2))
boxplot(stats$Tpot,main="Tpot (mm/yr)",ylim=c(0,max(stats$Tpot)))
boxplot(stats$Tact,main="Tact (mm/yr)",ylim=c(0,max(stats$Tpot)))
boxplot(stats$Ypot,main="Ypot (mm/yr)",ylim=c(0,max(stats$Ypot)))
boxplot(stats$Yact,main="Yact (mm/yr)",ylim=c(0,max(stats$Ypot)))
dev.off()

# extra WP vs caprise
filplt <- paste("./res/plot_capriserz_WP.png",sep="")
png(file = filplt, width = 1024, height = 768, pointsize = 16, bg = "white")
par(mfrow = c(2,1),mar = c(bottom = 4, left = 4, top = 3, right = 4),cex=1.1)
xlab <- paste("qvcup")
ylab <- paste("WP_Tact")
main <- paste("WP_Tact (kg/m3 DM) as f(qvcup (mm/yr)) at rz 1971-2015")
plot(x=stats$qvcuprz, y=stats$WP_Tact , ,ylim=c(0,ymaxWP),
		main=c(main),xlab=xlab,ylab=ylab ,cex.sub = 0.75)
xlab <- paste("qvcup")
ylab <- paste("WP_ETact")
main <- paste("WP_ETact (ltr/kg DM) as f(qvcup (mm/yr)) at rz 1971-2015")
plot(x=stats$qvcuprz, y=stats$WP_ETact , ,ylim=c(0,ymaxWP),
		main=c(main),xlab=xlab,ylab=ylab ,cex.sub = 0.75)
dev.off()
# extra WFP vs caprise
filplt <- paste("./res/plot_capriserz_WFP.png",sep="")
png(file = filplt, width = 1024, height = 768, pointsize = 16, bg = "white")
par(mfrow = c(2,1),mar = c(bottom = 4, left = 4, top = 3, right = 4),cex=1.1)
xlab <- paste("qvcup")
ylab <- paste("WFP_Tact")
main <- paste("WFP_Tact (ltr/kg DM) as f(qvcup (mm/yr)) at rz  1971-2015")
plot(x=stats$qvcuprz, y=stats$WFP_Tact , ,ylim=c(0,ymaxWFP),
		main=c(main),xlab=xlab,ylab=ylab ,cex.sub = 0.75)
xlab <- paste("qvcup")
ylab <- paste("WFP_ETact")
main <- paste("WFP_ETact (ltr/kg DM) as f(qvcup (mm/yr)) at rz 1971-2015")
plot(x=stats$qvcuprz, y=stats$WFP_ETact , ,ylim=c(0,ymaxWFP),
		main=c(main),xlab=xlab,ylab=ylab ,cex.sub = 0.75)
dev.off()
