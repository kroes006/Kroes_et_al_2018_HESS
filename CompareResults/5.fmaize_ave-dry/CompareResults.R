# set working directory wd; all other dirs are relative to the wd 
# setwd("d:\\Kroes\\Prive\\artikel_VertDiscr_Kroes_Supit_etal\\test\\CompareResults\\1.grass_dry-cr0")
wd <- getwd()
cat("working directory = ",wd, "\n")
t1 <- date()

# with GOF-functions: Mean Error (ME), Root Mean Square Error (RMSE), 
#                     Nash-Sutcliffe efficiency (NSE),  Index of Agreement (d)
#library(hydroGOF)      # hydro-packages by Mauricio Zambrano-Bigiarini [aut, cre]


# -- fix ranges 
Yactmin = 0
Yactmax = 10000
Tactmin = 0
Tactmax = 300
qvupmin = 0
qvupmax = 300
qvdnmin = 0
qvdnmax = 800

# -- read folder names
filefrom = "./CompareResults.csv.txt"
dircomp <- read.table(file = filefrom, header = TRUE) 

# read results
filefrom <- paste(dircomp[1,1],"stats.csv.txt", sep="")
res1     <- read.csv(file = filefrom, header = TRUE, as.is = TRUE) 
tmp      <- unlist(strsplit(x=filefrom,split="[\\]"))
name1    <-  tmp[length(tmp)-3]
filefrom <- paste(dircomp[2,1],'stats.csv.txt', sep="")
res2     <- read.csv(file = filefrom, header = TRUE, as.is = TRUE) 
tmp      <- unlist(strsplit(x=filefrom,split="[\\]"))
name2    <-  tmp[length(tmp)-3]
diffres <- res1
diffres[,5:15] <- res1[,5:15] - res2[,5:15]
diffres[,17:26] <- res1[,17:26] - res2[,17:26]

# stats
#PIname    <- c("Yact",        "Tact", "ETact","CapRise_rz","CapRise_rz2","CapRise_rzME","Percolation_rz","Leaching_bot")
PIname    <- c("Yact",       "Tact", "ETact","CapRise_rz","Percolation_rz","Seepage_bot","Leaching_bot")
PIunit    <- c("kg/ha/yr DM","mm/yr","mm/yr","mm",        "mm",            "mm",         "mm")
stats <- data.frame(PIname=PIname,name1=NA,name2=NA,diff=NA,diffperc=NA,PIunit=PIunit)
names(stats)[2] <- name1
names(stats)[3] <- name2
stats[1,2] <- round(mean(res1$Yact,na.rm=TRUE),2)
stats[1,3] <- round(mean(res2$Yact,na.rm=TRUE),2)
stats[1,4] <- round(stats[1,2]-stats[1,3],2)
stats[1,5] <- round(100*(stats[1,4]/stats[1,3]),2)
stats[2,2] <- round(mean(res1$Tact,na.rm=TRUE),2)
stats[2,3] <- round(mean(res2$Tact,na.rm=TRUE),2)
stats[2,4] <- round(stats[2,2]-stats[2,3],2)
stats[2,5] <- round(100*(stats[2,4]/stats[2,3]),2)
stats[3,2] <- round(mean(res1$ETact,na.rm=TRUE),2)
stats[3,3] <- round(mean(res2$ETact,na.rm=TRUE),2)
stats[3,4] <- round(stats[3,2]-stats[3,3],2)
stats[3,5] <- round(100*(stats[3,4]/stats[3,3]),2)
stats[4,2] <- round(mean(res1$qvcuprz,na.rm=TRUE),2)
stats[4,3] <- round(mean(res2$qvcuprz,na.rm=TRUE),2)
stats[4,4] <- round(stats[4,2]-stats[4,3],2)
stats[4,5] <- round(100*(stats[4,4]/stats[4,3]),2)
stats[5,2] <- round(mean(res1$qvcdnrz,na.rm=TRUE),2)
stats[5,3] <- round(mean(res2$qvcdnrz,na.rm=TRUE),2)
stats[5,4] <- round(stats[5,2]-stats[5,3],2)
stats[5,5] <- round(100*(stats[5,4]/stats[5,3]),2)
stats[6,2] <- round(mean(res1$qvbotupyr,na.rm=TRUE),2)
stats[6,3] <- round(mean(res2$qvbotupyr,na.rm=TRUE),2)
stats[6,4] <- round(stats[6,2]-stats[6,3],2)
stats[6,5] <- round(100*(stats[6,4]/stats[6,3]),2)
stats[7,2] <- round(mean(res1$qvbotdnyr,na.rm=TRUE),2)
stats[7,3] <- round(mean(res2$qvbotdnyr,na.rm=TRUE),2)
stats[7,4] <- round(stats[7,2]-stats[7,3],2)
stats[7,5] <- round(100*(stats[7,4]/stats[7,3]),2)
# write statistics to file ------------------------------------------------------------
zz <- file("stats_compare.csv", "w")
cat("File generated at",t1,file=zz,sep="\n")
cat("",file=zz,sep="\n")
write.table(stats,file=zz,sep=",",row.names = FALSE)
close(zz)

# plot comparison
# caprise 
# rz up
png(file = "UpwardFlux_dynrz_diff.png", width = 1024, height = 780, pointsize = 22, bg = "white")
par(mfrow = c(2, 1),mar = c(bottom = 2, left = 5, top = 2, right = 2))
main1 <- paste("Upward flux across bottom RootZone (mm / crop season)")
par(adj = 0)
xlab1 <- paste("Time")
boxplot((qvcuprz) ~ year, data=diffres, ylim=c(qvupmin,qvupmax), xlab=xlab1,ylab="", main=main1, las=1,
	cex.lab=1.5,cex.axis=1.5,cex.main=1.5)
xlab1 <- paste("Time")
boxplot((qvcuprz) ~ Namebofek2012,data=diffres, ylim=c(qvupmin,qvupmax), xlab=xlab1,ylab="", main="", las=1,
	cex.lab=1.5,cex.axis=1.5,cex.main=1.5)
dev.off()
# rz dn
png(file = "Percolationdynrz_downward_diff.png", width = 1024, height = 780, pointsize = 22, bg = "white")
par(mfrow = c(2, 1),mar = c(bottom = 2, left = 4, top = 2, right = 2))
main1 <- paste("Percolation downward across bottom RootZone (mm / crop season)")
xlab1 <- paste("Time")
boxplot((qvcdnrz) ~ year, data=diffres, ylim=c(qvdnmin,qvdnmax), xlab=xlab1,ylab="", main=main1, las=1,
	cex.lab=1.5,cex.axis=1.5,cex.main=1.5)
main1 <- paste("Percolation downward at bottom RootZone (mm/crop season)")
xlab1 <- paste("Time")
ylab1 <- paste("Percolationdiff (mm / crop season)")
boxplot((qvcdnrz) ~ Namebofek2012,data=diffres, ylim=c(qvdnmin,qvdnmax), xlab=xlab1,ylab=ylab1, main="", las=1,
	cex.lab=1.5,cex.axis=1.5,cex.main=1.5)
dev.off()

# seepage/leakage across bottom (loward boundary)
# bot up
png(file = "SeepageBBCYear_upward_diff.png", width = 1024, height = 780, pointsize = 22, bg = "white")
par(mfrow = c(2, 1),mar = c(bottom = 2, left = 5, top = 2, right = 2))
main1 <- paste("Seepage upward at bottom of Profile (mm/yr)")
xlab1 <- paste("Time")
ylab1 <- paste("Seepagediff (mm/yr)")
boxplot((qvbotupyr) ~ year, data=diffres, ylim=c(qvupmin,qvupmax), xlab=xlab1,ylab=ylab1, main=main1, las=1,
	cex.lab=1.5,cex.axis=1.5,cex.main=1.5)
main1 <- paste("Seepage upward at bottom of Profile (mm/yr)")
xlab1 <- paste("Time")
ylab1 <- paste("Seepagediff (mm/yr)")
boxplot((qvbotupyr) ~ Namebofek2012,data=diffres, ylim=c(qvupmin,qvupmax), xlab=xlab1,ylab=ylab1, main=main1, las=1,
	cex.lab=1.5,cex.axis=1.5,cex.main=1.5)
dev.off()
# bot dn
png(file = "LeachingBBCYear_downward_diff.png", width = 1024, height = 780, pointsize = 22, bg = "white")
par(mfrow = c(2, 1),mar = c(bottom = 2, left = 5, top = 2, right = 2))
main1 <- paste("Leaching downward at bottom Profile (mm/yr)")
xlab1 <- paste("Time")
ylab1 <- paste("Leachingdiff (mm/yr)")
boxplot((qvbotdnyr) ~ year, data=diffres, ylim=c(qvdnmin,qvdnmax), xlab=xlab1,ylab=ylab1, main=main1, las=1,
	cex.lab=1.5,cex.axis=1.5,cex.main=1.5)
main1 <- paste("Leaching downward at bottom Profile (mm/yr)")
xlab1 <- paste("Time")
ylab1 <- paste("Leachingdiff (mm/yr)")
boxplot((qvbotdnyr) ~ Namebofek2012,data=diffres, ylim=c(qvdnmin,qvdnmax), xlab=xlab1,ylab=ylab1, main=main1, las=1,
	cex.lab=1.5,cex.axis=1.5,cex.main=1.5)
dev.off()

# Yact
png(file = "Yact_diff.png", width = 1024, height = 780, pointsize = 22, bg = "white")
par(mfrow = c(2, 1),mar = c(bottom = 2, left = 5, top = 2, right = 2))
main1 <- paste("Difference in Yield (Yact) (kg/ha DM)")
xlab1 <- paste("Time")
ylab1 <- paste("Yact (kg/ha DM)")
boxplot(Yact ~ year, data=diffres, ylim=c(Yactmin,Yactmax), xlab=xlab1,ylab="", main=main1, las=1,
	cex.lab=1.5,cex.axis=1.5,cex.main=1.5)
main1 <- paste("Difference in Yield (Yact) (kg/ha DM)")
xlab1 <- paste("Time")
ylab1 <- paste("Yact_diff (kg/ha DM)")
boxplot(Yact ~ Namebofek2012, data=diffres, ylim=c(Yactmin,Yactmax), xlab=xlab1,ylab="", main="", las=1,
	cex.lab=1.5,cex.axis=1.5,cex.main=1.5)
dev.off()

# Tact
png(file = "Tact_diff.png", width = 1024, height = 780, pointsize = 22, bg = "white")
par(mfrow = c(2, 1),mar = c(bottom = 2, left = 5, top = 2, right = 2))
main1 <- paste("Tact (mm/yr)")
xlab1 <- paste("Time")
ylab1 <- paste("Tact_diff (mm/yr)")
boxplot(Tact ~ year,data=diffres, ylim=c(Tactmin,Tactmax), xlab=xlab1,ylab=ylab1, main=main1, las=1,
	cex.lab=1.5,cex.axis=1.5,cex.main=1.5)
ain1 <- paste("Tact (mm/yr)")
xlab1 <- paste("Time")
ylab1 <- paste("Tact_diff (mm/yr)")
boxplot(Tact ~ Namebofek2012,data=diffres, ylim=c(Tactmin,Tactmax), xlab=xlab1,ylab=ylab1, main=main1, las=1,
	cex.lab=1.5,cex.axis=1.5,cex.main=1.5)
dev.off()

# ETact
png(file = "ETact_diff.png", width = 1024, height = 780, pointsize = 22, bg = "white")
par(mfrow = c(2, 1),mar = c(bottom = 2, left = 5, top = 2, right = 2))
main1 <- paste("ETact (mm/yr)")
xlab1 <- paste("Time")
ylab1 <- paste("ETact_diff (mm/yr)")
boxplot(ETact ~ year,data=diffres, ylim=c(Tactmin,Tactmax), xlab=xlab1,ylab=ylab1, main=main1, las=1,
	cex.lab=1.5,cex.axis=1.5,cex.main=1.5)
main1 <- paste("ETact (mm/yr)")
xlab1 <- paste("Time")
ylab1 <- paste("ETact_diff (mm/yr)")
boxplot(ETact ~ Namebofek2012,data=diffres, ylim=c(Tactmin,Tactmax), xlab=xlab1,ylab=ylab1, main=main1, las=1,
	cex.lab=1.5,cex.axis=1.5,cex.main=1.5)
dev.off()

# plot CO2
# -- read 
#filefrom = "./Atmospheric.co2"
#dat <- read.table(file = filefrom, header = TRUE, skip=10) 
#png(file = "CO2.png", width = 1024, height = 780, pointsize = 22, bg = "white")
#main <- paste("CO2 (ppm)")
#xlab <- paste("Time")
#ylab <- paste("CO2 (ppm)")
#plot(x=dat$CO2year, y=dat$CO2ppm,
#		main=c(main),xlab=xlab,ylab=ylab ,cex.sub = 0.75)
#dev.off()
