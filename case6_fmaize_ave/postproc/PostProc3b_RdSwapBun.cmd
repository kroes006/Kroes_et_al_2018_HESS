rem   ----- capillary rise at dynamic depth bottom rootzone
rem md res_qv
rem cd res_qv
rem -----------------  dynamic depth of root zone
copy  ..\qvert\result.run.%1.swap.crz  result.crz
R --no-restore --no-save <..\RdSwapPlot.qv.crz.r  > RdSwapPlot.qv.crz.%1.r.log
ren  qvRZ_year.csv      qvRZ_year.%1.qv.dynrz.csv 
ren  qvRZ_month.csv     qvRZ_month.%1.qv.dynrz.csv 
ren  RdSwapBun.caprisedaily.png  RdSwapBun.caprisedaily.%1.qv.dynrz.png 
ren  RdSwapBun.caprise4.png      RdSwapBun.caprise4.%1.qv.dynrz.png 
del  result.crz
