@echo off
rem part of TEST procedure of SWAP3
del/q    result.*
REM del/q    fort.*
del/q    *.aun
del/q    *.afo
del/q    *.bfo
del/q    *.blc
del/q    *.bal
del/q    *.bun
REM del/q    *.csv
del/q    *.dat
del/q    *.dwb
del/q    *.inc
del/q    *.log
del/q    *.out
del/q    *.png
del/q    *.res
del/q    *.sba
del/q    *.snw
del/q    *.str
del/q    *.tem
del/q    *.tmp
del/q    *.vap
del/q    *.wba
del/q    swap.ok
REM del/q    PerformIndex.tex
REM del/q    PerformIndex2.tex
REM del/q    Waterbalans.tex
rem exit
