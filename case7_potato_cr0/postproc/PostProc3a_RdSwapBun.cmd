rem   ----- capillary rise at dynamic depth bottom rootzone 
md res_qv
cd res_qv

@echo off
:start
	set /a count+=1
	if %count%==73 goto end
	echo  count has value %count%
	call ..\PostProc3b_RdSwapBun %count%
goto start

:end
echo count has final value of %count%
pause
exit
